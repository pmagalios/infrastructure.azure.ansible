# Getting Started
* Install Python3
* pip3 install ansible
* pip3 install -r requirements-azure.txt
* Log in to azure `az login`
* Install the ansible-galaxy modules `ansible-galaxy install azure.azure_modules community.windows`
* Run against your test system(s) `ansible all -m setup -i myazure_rm.yml -e ansible_user=<Usr> -e ansible_password=<Passwd> -e ansible_connection=winrm -e ansible_winrm_server_cert_validation=ignore`
